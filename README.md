
# **Anniv Reminder**
![forthebadge](https://forthebadge.com/images/badges/built-for-android.svg)
![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)
![forthebadge](https://forthebadge.com/images/badges/open-source.svg)
<br></br>
Birthday management app for those who have too many friends to remember all birthdays, or who are afraid of private apps, this is made for you.

Enter in the app the name of someone and his birthday, and you will be prevent the day and also the day before through notifications in your smartphone.
All added day are displayed in a simple menu with the posibility to remove or modify then.
<br></br>
<br></br>

# Installation and start
To run the application, you need to download the file "[*annivreminder.apk*](https://gitlab.com/JumpyfrOg/anniv-reminder/-/blob/master/annivreminder.apk)" at the root of the project. Then executed it on a smartphone with the operating system android (by clicking on it in a file explorer through your smartphone).


# Software used

- Online photo editor : [Pixlr](https://pixlr.com/)
- IDE : [Android Studio](https://developer.android.com/stud0io)
- Online markdown editor : [Stackedit](https://stackedit.io/)


# Contact
- Mail : mcharron29@gmail.com
- Twitter : [Charron M](https://twitter.com/CharronM5)


# License

[GPL] License

 [GPL]: LICENSE
