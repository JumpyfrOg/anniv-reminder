package com.example.annivreminder;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

/**
 * The activity of the settings page. Display settings_main_layout and the settings fragment.
 *
 * @author Charron M.
 * @version 1.1
 * License : GPL or later
 **/
public class SettingsActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_main_layout);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.settings_title_toolbar));  // Change title of the toolbar

        // overide "back" button of the phone to recall the main activity and apply modification
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                // create a new intent to prepare switching activity
                Intent intent = new Intent(SettingsActivity.this,MainActivity.class);

                // put a flag to specify this activity is closed and go back to the previous which
                // is the main activity of the app
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                // start activity
                startActivity(intent);
            }
        };
        getOnBackPressedDispatcher().addCallback(this, callback);


        if ( findViewById(R.id.fragment_container) != null ) {
            if ( savedInstanceState != null ) {
                return;
            }
            getFragmentManager().beginTransaction().add(R.id.fragment_container,
                    new SettingsFragment()).commit();   // Display the settings fragment
        }

    }

}
