package com.example.annivreminder;

import android.content.Context;
import android.widget.Toast;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Class which contains methods to extract and save data from storage data file of the app.
 *
 * @author Charron M.
 * @version 1.1
 * License : GPL or later
 **/
public abstract class RWFile {


    /**
     * Read the file data.txt in the app, where are stored the couple saved, to extract the saved
     * couple and return it in a ArrayList. Require the context of the calling activity to get
     * access to app resources. The returned list contains a list of two-box string array.
     * The first box conteins the name, and the second the date in string format.
     * @param context the context of the calling activity
     * @return The list of saved couple
     */
    public static ArrayList<String[]> load(Context context) {
        ArrayList<String[]> ret = new ArrayList<>();
        FileInputStream fis = null;
        ObjectInputStream is = null;
        try {
            // Get the file where data are saved
            File directory = context.getFilesDir();
            File file = new File(directory,
                    context.getResources().getString(R.string.couple_file_name));

            boolean cont = true;

            // Create stream to pass data from the file through the application
            fis = new FileInputStream(file);
            is = new ObjectInputStream(fis);

            // if the reader isn't at the end of the file
            while (cont) {
                // get the next object wrote in the file
                Object obj = is.readObject();
                if (obj != null) {
                    ret.add((String[])obj);
                } else {
                    cont = false;
                }
            }

        // If a error occur
        }catch (FileNotFoundException e) {
            //normaly expeted at the first launch
            //If a exception is throw during the lecture of the file
        } catch (EOFException e) {
            //normaly happen at end of the file
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Erreur du chargement des données",
                    Toast.LENGTH_LONG).show();
        }
        finally {
            // close  stream
            try {
                if (is != null) {
                    is.close();
                }
                if (fis != null) {
                    fis.close();
                }
            }catch (Exception ignored){}

        }
        return ret;
    }


    /**
     * Save the list passed by parameters in the file of data of the app, with the name saved in
     * "couple_file_name"  string value. The list must be contains array of string that represent a
     * couple of name-date in same organisation that  the returned list of load(). Require the
     * context of the calling activity to access to the app resources.
     * @param list The list to save in the file
     * @param context the context of the calling activity
     */
    public static void save( ArrayList<String[]> list, Context context){
        // save all couple name-date as well as the new
        FileInputStream fis = null;
        ObjectInputStream is = null;
        try {
            // get the file to store couple, or create it
            File directory = context.getFilesDir();
            File file = new File(directory,
                    context.getResources().getString(R.string.couple_file_name));

            // create stream to pass data between the file and the app
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            // for each couple, save one by one in the file
            for ( String[] couple : list ) {
                oos.writeObject(couple);
            }
            // inform the user that the save is successful
            Toast.makeText(context, "Modification effectuées",Toast.LENGTH_SHORT).show();

        // if a exception is throw during the saving, inform the user that the backup didn't work
        }catch(Exception e){
            Toast.makeText(context,
                    "Echec de la sauvegarde définitive",Toast.LENGTH_LONG).show();
        }
        finally {
            // close  stream
            try {
                if (is != null) {
                    is.close();
                }
                if (fis != null) {
                    fis.close();
                }
            }catch (Exception ignored){}

        }
    }
}
