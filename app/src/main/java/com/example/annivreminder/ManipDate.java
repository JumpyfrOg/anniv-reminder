package com.example.annivreminder;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Contains method to manipulate and test date.
 *
 * @author Charron M.
 * @version 1.1
 * License : GPL or later
 **/
public abstract class ManipDate {

    /**
     * Check if the passed date point to the today date (today at the moment of the execution
     * of the method) by compare it with the todayw date. Return true if it is.
     * @return True if the date point to today date.
     */
    public static boolean isToday (Date isIt) {
        // get the today date
        Calendar myCalendar = Calendar.getInstance();   // initialize at today by default
        Date date_today = myCalendar.getTime();
        return compareDate(isIt,date_today) == 0;
    }


    /**
     * Check if the passed date point to the tomorrow date (tomorrow at the moment of the execution
     * of the method) by compare it with the tomorrow date. Return true if it is.
     * @return True if the date point to tomorrow date.
     */
    public static boolean isTomorrow (Date isIt) {
        // get the today date and add one date at the calendar
        Calendar myCalendar = Calendar.getInstance();   // initialize at today by default
        myCalendar.add(Calendar.DAY_OF_MONTH,1);
        Date date_tomorrow = myCalendar.getTime();
        return compareDate(isIt,date_tomorrow) == 0;
    }


    /**
     * Compare the two date (compare the month and the day). Return 0 if the dates are equals,
     * negative number if the first date passed by parameters is before the second, else positive
     * value is the opposite case
     * @param d1 One date to test
     * @param d2 The second date to test to
     * @return 0 if equals, negative if d1 is before d2, positive if d1 is after d2.
     */
    public static int compareDate(Date d1, Date d2) {
        int ret = -1;
        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(d2);
        if ( c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) ||
                (c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) &&
                c1.get(Calendar.DAY_OF_MONTH) > c2.get(Calendar.DAY_OF_MONTH)) ) {
            ret = 1;
        }else if ( c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) &&
                c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH) ) {
            ret = 0;
        }
        return ret;
    }


    /**
     * Sort the list of couple name-date passed by parameters by date. Uses compareDate() so
     * the years of the dates in the list are not considered in the sort process. Requires the
     * context of the calling activityto get access to "pattern_parse-date" string value.
     * @param list The list of couple to sort
     * @param context the context of the calling activity
     */
    public static void sortList(ArrayList<String[]> list, Context context) {
        int minDate_index;
        // get the minimal date, move it at the end of the list. This for each elem of the list
        // begin scan and move from the end of the list
        for ( int i = 0; i < list.size(); i++ ) {
            minDate_index = list.size() - (i+1);

            // convert string date to date obj, date of the last obj not sort of the list
            Calendar cal = Calendar.getInstance();
            try{cal.setTime(new SimpleDateFormat(context.getResources()
                    .getString(R.string.pattern_cmp_date)).parse(list.get(minDate_index)[1]));}
            catch(Exception e){System.out.println(e);}
            Date dMin = cal.getTime();

            for ( int j = minDate_index-1; j >= 0 ; j-- ) {
                // convert string date to date obj
                try{cal.setTime(new SimpleDateFormat(context.getResources()
                        .getString(R.string.pattern_cmp_date)).parse(list.get(j)[1]));}
                catch(Exception e){System.out.println(e);}
                Date dParcours = cal.getTime();

                // save the min date of the list
                if ( ManipDate.compareDate(dParcours,dMin) < 0 ) {
                    minDate_index = j;
                    // convert string date to date obj, date of the last obj not sort of the list
                    cal = Calendar.getInstance();
                    try{cal.setTime(new SimpleDateFormat(context.getResources()
                            .getString(R.string.pattern_cmp_date)).parse(list.get(minDate_index)[1]));}
                    catch(Exception e){System.out.println(e);}
                    dMin = cal.getTime();
                }
            }

            // move the min date to the end of the list
            list.add(list.get(minDate_index));
            list.remove(minDate_index);

        }
    }


    /**
     *  Convert date passed by parameters in the format selected from preferences. By default, the
     *  format is the compare pattern. The context of the calling activity is required to get string
     *  ressources.
     * @param context The context of the calling activity
     * @param date The date in string format
     * @return The string converted
     */
    public static String getDateInFormat(Context context, String date) {
        String ret = null;
        // get the pattern selected in parameters
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String pattern = sharedPref.getString(context.getString(R.string.settings_pattern_date_key),
                context.getResources().getString(R.string.pattern_cmp_date));
        Calendar cal = Calendar.getInstance();

        // if the choosen pattern is the "Jour Mois"
        if ( pattern.equals("jour_mois") ) {
            // convert string to date obj with the cmp pattern
            try{cal.setTime(new SimpleDateFormat(
                    context.getResources().getString(R.string.pattern_cmp_date)).parse(date));}
            catch(Exception e){System.out.println(e);}
            // get the name of the month
            String[] mounth = context.getResources().getStringArray(R.array.month_french);
            ret = cal.get(Calendar.DAY_OF_MONTH) + " " + mounth[cal.get(Calendar.MONTH)];

        }else{  // else the pattern can be use to parse the date
            // convert string to date obj
            try{cal.setTime(new SimpleDateFormat(pattern).parse(date));}
            catch(Exception e){System.out.println(e);}

            // convert date obj to string by using the selected pattern
            SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.FRANCE);
            ret = sdf.format(cal.getTime());
        }

        return ret;
    }

}
