package com.example.annivreminder;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Class which create and put in the list view a item for each couple that is given through the
 * constructor.
 *
 * @author Charron M.
 * @version 1.1
 * License : GPL or later
 **/
public class AdapterList extends BaseAdapter {

    // The list of couple to display
    private ArrayList<String[]> liste_couple;

    // The context of the application where the display should take place
    private Context context;

    private LayoutInflater inflater;

    /**
     * Constructor of the class
     * @param context The context where display the list
     * @param list The list of couple to display
     */
    public AdapterList(Context context, ArrayList<String[]> list) {
        this.context = context;
        this.liste_couple = sortList(list);
        inflater = LayoutInflater.from(context);
    }

    public int getCount () {
        return this.liste_couple.size();
    }

    /**
     * Method which return the context where the adapter adpat the display of the list
     * @return Context of the adapter
     */
    public Context getContext () {
        return this.context;
    }

    public Object getItem(int position){
        ArrayList<String[]> list = RWFile.load(context);
        int i = 0;
        String[] ret = null;
        while ( ret == null && i < list.size() ) {
            if ( list.get(i)[0].equals(this.liste_couple.get(position)[0])
                    && list.get(i)[1].equals(liste_couple.get(position)[1]) ) {
                ret = list.get(i);
            }
            i++;
        }
        return ret;
    }

    public long getItemId(int position){
        ArrayList<String[]> list = RWFile.load(context);
        int pos = -1, i = 0;
        while ( pos == -1 && i < list.size() ) {
            if ( list.get(i)[0].equals(this.liste_couple.get(position)[0])
                    && list.get(i)[1].equals(liste_couple.get(position)[1]) ) {
                pos = i;
            }
            i++;
        }
        return pos;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            view = (View) inflater.inflate(R.layout.list_item, parent, false);
        }else{
            view = (View) convertView;
        }


        // get the component to display a element of the list, so a couple
        ((TextView) view.findViewById(R.id.item_date)).setText(ManipDate.getDateInFormat(
                context,this.liste_couple.get(position)[1]));
        ((TextView) view.findViewById(R.id.item_nom)).setText(this.liste_couple.get(position)[0]);


        // set a background color according the position of the day from today or tomorrow
        // if colors or enabled
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        if ( sharedPref.getBoolean(
                context.getResources().getString(R.string.settings_color_key),false) ) {
            Calendar cal = Calendar.getInstance();
            Date today = cal.getTime();
            try {
                cal.setTime(new SimpleDateFormat(context.getResources()
                        .getString(R.string.pattern_cmp_date)).parse(this.liste_couple.get(position)[1]));
            } catch (Exception e) {
                System.out.println(e);
            }
            Date date = cal.getTime();

            LinearLayout lin = (LinearLayout) view.findViewById(R.id.layout_item);
            if (ManipDate.compareDate(date, today) >= 0) {
                if (ManipDate.isTomorrow(date)) {
                    lin.setBackgroundColor(context.getResources().getColor(R.color.tomorrow_color, null));
                } else if (ManipDate.isToday(date)) {
                    lin.setBackgroundColor(context.getResources().getColor(R.color.today_color, null));
                } else {
                    lin.setBackgroundColor(context.getResources().getColor(R.color.soon_color, null));
                }
            } else {
                lin.setBackgroundColor(context.getResources().getColor(R.color.white, null));
            }
        }
        return view;
    }


    /**
     * Return a copy of the list passed by parameters. The copy is sorted according the mode
     * selected in settings.
     * @param list List of couple to copy and sorted
     */
    private ArrayList<String[]> sortList(ArrayList<String[]> list) {
        // copy the list
        ArrayList<String[]> ret = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            // check saveAction() of AddActivity for ordering of a couple
            ret.add(new String[]{list.get(i)[0], list.get(i)[1], i+""});
            // the list of ordering couple got also the index in the initial list
        }

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.context);
        String mode = sharedPref.getString(context.getString(R.string.settings_sort_key),
                "default");
        if ( mode.equals("date") ) {
            ManipDate.sortList(ret,this.context);
        }else if ( mode.equals("name") ) {
            this.sortByName(ret);
        }
        return ret;
    }


    /**
     * Sort the list passed by parameters according the name in the couple. The element of the list
     * are sorted in ascending order.
     */
    private void sortByName(ArrayList<String[]> list_to_sort){
        int minDate_index;
        // get the minimal date, move it at the end of the list. This for each elem of the list
        // begin scan and move from the end of the list

        for ( int i = 0; i < list_to_sort.size(); i++ ) {
            minDate_index = list_to_sort.size() - (i+1);

            String name1 = list_to_sort.get(minDate_index)[0];

            for ( int j = minDate_index-1; j >= 0 ; j-- ) {
                String name2 = list_to_sort.get(j)[0];

                // save the min date of the list
                if ( name1.toLowerCase().compareTo(name2.toLowerCase()) > 0 ) {
                    minDate_index = j;
                    name1 = list_to_sort.get(minDate_index)[0];
                }
            }

            // move the min date to the end of the list
            list_to_sort.add(list_to_sort.get(minDate_index));
            list_to_sort.remove(minDate_index);
        }
    }
}
