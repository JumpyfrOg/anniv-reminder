package com.example.annivreminder;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Activity to add a new couple of name-date, or to modify an existing one.
 *
 * @author Charron M.
 * @version 1.1
 * License : GPL or later
 **/
public class AddActivity extends AppCompatActivity {

    // calendar that got the selected date
    final Calendar myCalendar = Calendar.getInstance();

    // boolean to say if the activity was launched for add a new couple
    private boolean toAdd;

    // if the activity has the goal to modify a existing couple, the index of his position in the
    // list of couple is saved in this variable
    private int index = -1;

    // the name and date before modification
    private String oldName;
    private Date oldDate;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_layout);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);


        // get data pass through activity, data from the mainActiviy of the app
        Bundle bundle = getIntent().getExtras();
        if ( bundle.containsKey("action") ) {
            // initialize toAdd variable
            if ( bundle.getString("action").equals("add") ) {
                toAdd = true;

            // if is for modification, get additional information
            }else{
                //get the position of the selected couple in the list
                // and set his data on the view to modify it.
                toAdd = false;
                if ( bundle.containsKey("position") ) {
                    this.index = bundle.getInt("position");
                    ArrayList<String[]> list = RWFile.load(this);
                    // name
                    ((EditText)findViewById(R.id.name_input)).setText(list.get(this.index)[0]);
                    this.oldName = list.get(this.index)[0];
                    // date
                    try{myCalendar.setTime(
                            new SimpleDateFormat(getResources().getString(R.string.pattern_cmp_date)).parse(list.get(this.index)[1]));}
                    catch(Exception e){System.out.println(e);}
                    this.oldDate = myCalendar.getTime();
                }
            }
        }


        // DATE BUTTON
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // update date pointed of calendar
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDateButton();
            }

        };
        Button but = (Button) findViewById(R.id.date_but);
        but.setBackgroundColor(getResources().getColor(R.color.default_grey,null));
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // display a date picker init to the selected date (today by default)
                new DatePickerDialog(AddActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        // SAVE BUTTON
        but = (Button) findViewById(R.id.save_but);
        but.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                saveAction();
            }
        });


        // CANCEL BUTTON
        but = (Button) findViewById(R.id.cancel_button) ;
        but.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                cancelAction();
            }
        });


        // DELETE BUTTON
        but = (Button) findViewById(R.id.supp_button);
        if ( !this.toAdd ) {    // disabled button if is not a modification
            but.setEnabled(true);
        }
        but.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                deleteAction();
            }
        });


        // Display the default date on the date button
        this.updateDateButton();
    }


    /**
     * Set the selected date on date picker in the label of the date button.
     */
    private void updateDateButton() {
        SimpleDateFormat sdf = new SimpleDateFormat(
                getResources().getString(R.string.pattern_cmp_date), Locale.FRANCE);
        Button but= (Button) findViewById(R.id.date_but);
        but.setText(getResources().getString(R.string.date_button)+" : "+sdf.format(myCalendar.getTime()));
    }


    /**
     * Method execute when the save button has been pressed. Check if the entered name is valid,
     * display a warning if not, else the couple name-date is saved in a file. The notification of
     * the app is updated if necessary
     */
    public void saveAction () {
        // display error message if the name is not entered
        if ( ((EditText)findViewById(R.id.name_input)).getText().toString().equals("")
            || ((EditText)findViewById(R.id.name_input)).getText().toString().equals(" ") ) {
            ((TextView)findViewById(R.id.warning_name)).setVisibility(View.VISIBLE);
        }else{
            ((TextView)findViewById(R.id.warning_name)).setVisibility(View.INVISIBLE);

            ArrayList<String[]> list = RWFile.load(this);
            String myFormat = getResources().getString(R.string.pattern_cmp_date);
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.FRANCE);
            if ( this.toAdd ) { // add mode
                // add a couple in the list
                list.add(new String[] {
                        ((EditText)findViewById(R.id.name_input)).getText().toString(),
                        sdf.format(myCalendar.getTime())
                });
            }else{  // modif mode
                // modify the existing couple
                list.get(this.index)[0] =
                        ((EditText)findViewById(R.id.name_input)).getText().toString();
                list.get(this.index)[1] = sdf.format(myCalendar.getTime());
            }

            // save the modified list
            RWFile.save(list,this);


            // update the displayed notification if necessary
            // if is to modify, and the notif contains the name before modification
            if ( !this.toAdd && ManagerNotifActivity.content_notif.contains(this.oldName) ) {
                // if the name or the date has been modified
                if (ManipDate.compareDate(this.oldDate,myCalendar.getTime()) != 0 ||
                        !this.oldName.equals(((EditText) findViewById(R.id.name_input))
                                .getText().toString())) {
                    ManagerNotifActivity.requestNotif(this);
                }

            // else add mode
            } else {
                // update notification if the new date is today or tomorrow
                if (ManipDate.isToday(myCalendar.getTime()) ||
                        ManipDate.isTomorrow(myCalendar.getTime())) {
                    ManagerNotifActivity.requestNotif(this);
                }
            }

            this.backToMain();
        }
    }


    /**
     * Method executed when cancel button is pressed. Change activity to go back to the main
     * activity
     */
    public void cancelAction () {
        this.backToMain();
    }


    /**
     * Method executed when delete button is pressed. Get the list of couple, remove the selected
     * couple, update notification if necessary and save the list before come back to the main
     * activity.
     */
    public void deleteAction () {
        ArrayList<String[]> list = RWFile.load(this);
        list.remove(this.index);
        RWFile.save(list,this);
        // if the deleted couple has his named displayed in the notification
        if ( !this.toAdd && ManagerNotifActivity.content_notif.contains(this.oldName) ) {
            ManagerNotifActivity.requestNotif(this);
        }
        this.backToMain();
    }


    /**
     * Create a intent with "clear top" flag to come back to the main activity and start it.
     */
    private void backToMain() {
        // create a new intent to prepare switching activity
        Intent intent = new Intent(AddActivity.this,MainActivity.class);

        // put a flag to specify this activity is closed and go back to the previous which is the
        // main activity of the app
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // start activity
        startActivity(intent);
    }
}
