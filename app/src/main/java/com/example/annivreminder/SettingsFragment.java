package com.example.annivreminder;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatDelegate;

/**
 * Fragment of preference list.
 *
 * @author Charron M.
 * @version 1.1
 * License : GPL or later
 **/

public class SettingsFragment extends PreferenceFragment{

    /**
     * Redefinition of depreciated default constructor
     */
    public SettingsFragment(){super();}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        // disabled boot preference if notiffications are not allow
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if ( !sharedPref.getBoolean(getResources().getString(R.string.settings_notif_all_key),false) ) {
            Preference notif_boot = findPreference(getResources().getString(R.string.settings_notif_boot_key));
            notif_boot.setEnabled(false);
        }


        // disabled or enabled boot preference when notifications preferences are pressed
        Preference notif_all = (Preference) findPreference(getResources().getString(R.string.settings_notif_all_key));
        notif_all.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                Preference notif_boot = findPreference(getResources().getString(R.string.settings_notif_boot_key));
                notif_boot.setEnabled((boolean)o);
                ManagerNotifActivity.requestNotif(getActivity(),(boolean)o);
                return true;
            }
        });
    }


    /**
     * FOR DEBUG
     * Print in log all keys saved from the preferences screen.
     */
//    public static void mapSharePreference(Context context){
//        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
//        Map<String, ?> allEntries = sharedPref.getAll();
//        Log.i(" - - - - - - - Shares preference values ",":");
//        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
//            Log.i(" - - - - - - - map values", entry.getKey() + ": " + entry.getValue().toString()+" - - - - - - - ");
//        }
//    }

}
