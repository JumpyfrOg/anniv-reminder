package com.example.annivreminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Class which react to armed alarm by call the method to display notifications
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        ManagerNotifActivity.requestNotif(context);
    }
}
