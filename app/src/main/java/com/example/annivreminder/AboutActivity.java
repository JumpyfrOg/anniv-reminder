package com.example.annivreminder;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

/**
 * Activity to display the about page.
 *
 * @author Charron M.
 * @version 1.1
 * License : GPL or later
 **/
public class AboutActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_layout);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.about_title_toolbar));  // Change title of the toolbar

        // Add link action on the text view to open them in a browser
        ((TextView) findViewById(R.id.tw_contact_content)).setMovementMethod(LinkMovementMethod.getInstance());
        ((TextView) findViewById(R.id.tw_projet_content)).setMovementMethod(LinkMovementMethod.getInstance());
    }
}
