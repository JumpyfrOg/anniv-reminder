package com.example.annivreminder;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import androidx.core.app.NotificationCompat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Class which manipulate the notifications of the app. Class also called at the end of the phone's
 * boot to show notification.
 *
 * @author Charron M.
 * @version 1.1
 * License : GPL or later
 **/
public class ManagerNotifActivity extends BroadcastReceiver {

    // the manager of notification of the app
    public static NotificationManager notifMana;
    // the notification builder of the app
    public static NotificationCompat.Builder notifBuild;

    // the text displayed in the notification
    public static String content_notif = "";

    // the id of the notification of the app
    private final static int notif_id = 0;

    // list of name displayed in the notification (for today birthday)
    private static ArrayList<String> notif_today = new ArrayList<>();

    // list of name displayed in the notification (for tomorrow birthday)
    private static ArrayList<String> notif_tomorrow = new ArrayList<>();


    @Override
    // method executed at end of the phone's boot
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            // request notification if boot notifications are allowed
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
            boolean boot_allow = sharedPref.getBoolean(context.getResources()
                    .getString(R.string.settings_notif_boot_key),false);
            if ( boot_allow ) {
                requestNotif(context);
            }

            // create a alarm request to check dates when the day change (check at midnight)
            armingAlarm(context);
        }
    }


    /**
     * Call load() to get couple saved, sort the today dates and tomorrow dates; then create a
     * notification with title and content according to the content of the lists of today/tomorrow
     * date if notification are activated in the app settings. Required the context of the calling
     * activity to get resources for load().
     * @param context The context of calling activity
     */
    private static void createNotif (Context context) {

        // the list of couple
        ArrayList<String[]> list = RWFile.load(context);

        // the list of couple where the date is today
        ArrayList<Integer> today_list = new ArrayList<>();
        // the list of couple where the date is tomorrow
        ArrayList<Integer> tomorrow_list = new ArrayList<>();


        // sort date in list if birthday is near today (today or tomorrow)
        SimpleDateFormat sdf = new SimpleDateFormat(context.getResources().getString(R.string.pattern_cmp_date));
        for (int i = 0; i < list.size(); i++) {
            Date date1 = null;
            try {
                date1 = sdf.parse(list.get(i)[1]);
            } catch (Exception e) {
            }

            if (ManipDate.isTomorrow(date1)) {
                // add date to tomorrow list if the date point to tomorrow
                tomorrow_list.add(i);
            } else if (ManipDate.isToday(date1)) {
                today_list.add(i);  // add date to today list if the date point to today
            }
        }


        // if the names of today's and tomorrow's birthdays are different from those displayed in
        // the notifications
        if ( requiredChangeNotif(today_list, tomorrow_list, list) ) {
            // if there is a birthday date to prevent (date of today or tomorrow) and the actual
            // notification must be recreate
            if ( list.size() > 0 && (today_list.size() != 0 || tomorrow_list.size() != 0)) {

                // INIT TITLE
                // set the title of the notification according to date to prevent
                String title = "Anniversaire ";
                if (today_list.size() != 0) {
                    title += "aujourd'hui ";
                    if (tomorrow_list.size() != 0) {
                        title += "et demain ";
                    }
                } else {
                    title += "demain ";
                }
                title += "!!";


                // INIT CONTENT
                // set the message of the notification
                String content = "";
                if (today_list.size() != 0) { // for today
                    content += "Aujourd'hui : ";
                    for (int i : today_list) {
                        content += list.get(i)[0] + ", ";
                    }
                    content = content.substring(0, content.length() - 2);  // remove the last ", "
                    content += ".";
                }

                // if there is today and tomorrow birthday, add a \n between today and tomorrow line
                if (today_list.size() != 0 && tomorrow_list.size() != 0) {
                    content += "\n";
                }

                if (tomorrow_list.size() != 0) {  // for tomorrow
                    content += "Demain : ";
                    for (int i : tomorrow_list) {
                        content += list.get(i)[0] + ", ";
                    }
                    content = content.substring(0, content.length() - 2);  // remove the last ", "
                    content += ".";
                }


                // INIT NOTIFICATION
                // initialize notification manager of the app
                String CHANNEL_ID = context.getString(R.string.channel_id);
                CharSequence nameChannel = context.getString(R.string.channel_name);
                String description = context.getString(R.string.channel_description);
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
                NotificationChannel channel =
                        new NotificationChannel(CHANNEL_ID, nameChannel, importance);
                channel.setDescription(description);
                if (notifMana == null) {
                    notifMana = context.getSystemService(NotificationManager.class);
                    notifMana.createNotificationChannel(channel);
                }

                // create an Intent for activate the mainActivity on click
                Intent new_intent = new Intent(context, MainActivity.class);
                // create the TaskStackBuilder and add the intent, which inflates the back stack
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                stackBuilder.addNextIntentWithParentStack(new_intent);
                // get the PendingIntent containing the entire back stack
                PendingIntent new_pending_intent =
                        stackBuilder.getPendingIntent(0,
                                PendingIntent.FLAG_UPDATE_CURRENT);

                // initialize the notification to build and display
                NotificationCompat.Builder builder =
                        new NotificationCompat.Builder(context, CHANNEL_ID)
                                .setSmallIcon(R.drawable.ic_stat_name)
                                .setContentIntent(new_pending_intent)
                                .setContentTitle(title)
                                .setOngoing(true)
                                .setStyle(new // multi-line notification
                                        NotificationCompat.BigTextStyle().bigText(content));
                // display the notification on the phone
                notifMana.notify(notif_id, builder.build());

                //save notification builder, manager and content
                notifBuild = builder;
                content_notif = content;


                // else remove the notification, if there is one
            } else {
                if (notifMana != null) {
                    notifMana.cancel(notif_id);
                    notif_today = new ArrayList<>();
                    notif_tomorrow = new ArrayList<>();
                }
            }
        }
    }


    /**
     * Request the creation or cancellation of notifications by call the other requestNotif function
     * by passing the value of the activation of notifications saved in preferences. The context of
     * the activity is required to manipulate notifications and get saved preferences.
     * @param context The context of the activity
     */
    public static void requestNotif ( Context context ) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        requestNotif(context,sharedPref.getBoolean(
                context.getResources().getString(R.string.settings_notif_all_key),false));
    }

    /**
     * Create a new notification if true is passed by parameters else the notification were deleted.
     * Call createNotif function to create it. The context of the activity is required to manipulate
     * notification.
     * @param context The context of the activity
     * @param isEnabled True to create a notification, else false for delete it
     */
    public static void requestNotif ( Context context, boolean isEnabled ) {
        if ( isEnabled ) {
            createNotif(context);
        }else{
            // remove notification if there is one
            if ( notifMana != null ) {
                notifMana.cancel(notif_id);
                notif_today = new ArrayList<>();
                notif_tomorrow = new ArrayList<>();
            }
        }
    }


    /**
     * Check if the names in the lists passed by parameters are different that the names displayed
     * in the notifications (names saved in "notif_today" and "notif_tomorrow" variables). Return
     * if the names are differents and a new notification is required, false else. The lists
     * don't contains the names but their number. The list which contains all names with their
     * number is required to link the two first lists.
     * @param today The list of couple of birthday to display
     * @param tomorrow The list of new names of tomorrow to display
     * @param list The list which contains the names
     * @return True if the lists today/notif_today and tomorrow/notif_tomorrow are the same.
     */
    private static boolean requiredChangeNotif
            (ArrayList<Integer> today, ArrayList<Integer> tomorrow, ArrayList<String[]> list) {
        boolean ret = true;

        // copy the name of today and tomorrow birthday
        ArrayList<String> tmp_notif_today = new ArrayList<>();
        for( Integer i : today ) {
            tmp_notif_today.add(list.get(i)[0]);
        }
        ArrayList<String> tmp_notif_tomorrow = new ArrayList<>();
        for( Integer i : tomorrow ) {
            tmp_notif_tomorrow.add(list.get(i)[0]);
        }

        // no notification already displayed
        if ( (notif_today == null || notif_today.size() == 0) && (notif_tomorrow == null || notif_tomorrow.size() == 0) ) {
            // save the list will become list of birthdays displayed in notification
            notif_today = tmp_notif_today;
            notif_tomorrow = tmp_notif_tomorrow;
        }else if ( listContains(notif_today, tmp_notif_today) && listContains(notif_tomorrow, tmp_notif_tomorrow) ) {
            ret = false;
        }else{
            notif_today = tmp_notif_today;
            notif_tomorrow = tmp_notif_tomorrow;
        }
        return ret;
    }


    /**
     * Check if the two list passed by parrameters have the same content. The content may not be
     * organized in the lists.
     * @param f_list The first list to check
     * @param s_list The second list to check
     * @return True if the two list have the same content
     */
    private static boolean listContains (ArrayList<String> f_list, ArrayList<String> s_list) {
        boolean ret = true;
        for ( int i = 0 ; i < f_list.size() && ret ; i++ ) {
            boolean find = false;
            for ( int j = 0 ; j < s_list.size() && !find ; j++ ) {
                if ( f_list.get(i).equals(s_list.get(j)) ) {
                    find = true;
                }
            }
            if ( !find ) {
                ret = false;
            }
        }
        if ( ret ) {
            if ( f_list.size() != s_list.size() ) {
                ret = false;
            }
        }
        return ret;
    }


    /**
     * Check if a notification from the app is already displayed on the phone. If not, a
     * notification is called through requestNotif() to display one.
     * @param context The context of the calling activity
     */
    public static void checkNotifAtBoot(Context context) {
        if (notifMana == null){
            // get notification manager
            notifMana = context.getSystemService(NotificationManager.class);
        }
        // check if a notification from this app is already displayed
        if ( notifMana.getActiveNotifications().length == 0 ) {
            requestNotif(context);
        }
    }


    /**
     *  Arming an alarm to call the class AlarmReciever at 00:03 am of each day. Need the context to
     *  get the alarm service of the app.
     * @param context The context of the app.
     */
    public static void armingAlarm (Context context) {
        Intent i = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Calendar myCalendar = Calendar.getInstance();   // initialize at today by default
        myCalendar.setTimeInMillis(System.currentTimeMillis());
        myCalendar.add(Calendar.DAY_OF_MONTH,1);
        myCalendar.set(Calendar.HOUR_OF_DAY, 0);
        myCalendar.set(Calendar.MINUTE, 3);
        myCalendar.set(Calendar.SECOND, 0);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                myCalendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
    }
}
