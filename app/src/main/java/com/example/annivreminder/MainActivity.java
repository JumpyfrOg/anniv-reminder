package com.example.annivreminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;

import android.os.SystemClock;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Main activity of tha app, start at the launch at the app. Contains a list of couple that can be
 * selected to be modify and a floating button to add a new couple.
 *
 * @author Charron M.
 * @version 1.1
 * License : GPL or later
 **/
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // request notif at the beginning of the app
        ManagerNotifActivity.checkNotifAtBoot(this);

        // get the saved couple
        ArrayList<String[]> list = RWFile.load(this);

        // DEBUG List
//        list = new ArrayList<>();
//        list.add(new String[]{"2.5","02/01/10"});
//        list.add(new String[]{"1","01/01/10"});
//        list.add(new String[]{"3","03/01/10"});
//        list.add(new String[]{"2","02/01/10"});


        // create a adapter to display the couple on the screen
        AdapterList arrayAdap = new AdapterList(this,list);

        // put the layout of the list of couple
        ListView view = findViewById(R.id.listview_date);
        view.setAdapter(arrayAdap);

        // mess up the list to the original and default sort, to avoid mismatch problem
        list = RWFile.load(this);
        // init listener on each item
        this.initItemListerner(list);


        // if the list of couple is not empty (if there is saved couple)
        if ( list.size() > 0 ) {
            ((TextView)findViewById(R.id.empty_message)).setVisibility(View.INVISIBLE);
        }


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAction();
            }
        });


        // arming the alarm
        ManagerNotifActivity.armingAlarm(this);
    }


    // Overide methods to listener and adapter of item in the list

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            // launch settings like a new activity
            Intent intent = new Intent(MainActivity.this,SettingsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }else if (id == R.id.action_author) {
            // launch settings like a new activity
            Intent intent = new Intent(MainActivity.this,AboutActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Method to initialize the reaction of the app to a click on the couple display on the view
     */
    public void initItemListerner ( ArrayList<String[]> list ) {
        // get the view of the application
        ListView view = findViewById(R.id.listview_date);

        // add a action to a click from the user on a couple
        view.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                // get the couple selected by the user
                String[] selectedItem = (String[]) adapter.getItemAtPosition(position);

                // get the position of the couple in the list of couple
                // (in case of the displayed list is not order like the saved list)
                int i = 0;
                boolean find = false;
                while ( i < list.size() && !find ) {
                    String[] couple = list.get(i);
                    if ( couple[0].equals(selectedItem[0]) && couple[1].equals(selectedItem[1]) ) {
                        find = true;
                        i--;
                    }
                    i++;
                }

                // if the couple selected is in the list
                if ( find ) {
                    // create a new intent to prepare switching activity
                    Intent intent = new Intent(MainActivity.this,AddActivity.class);
                    // put in the switching intent, the type of the action done. "Modif" if the next
                    // activity must modify the selected couple, "Add" if the next activity must add
                    // a new couple
                    intent.putExtra("action","modif");
                    // put the position of the selected couple in this list
                    intent.putExtra("position",i);
                    // put a flag to specify the next activity like a new activity on this activity
                    // the menu of the app
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    // start the new activity
                    startActivity(intent);
                }
            }
        });
    }


    /**
     * Method executed when the floating button is pressed. Switch to AddActivity by passing "add"
     * extra to "action" key to inform that user wants to add a new couple.
     */
    public void addAction () {
        // create a new intent to prepare switching activity
        Intent intent = new Intent(MainActivity.this,AddActivity.class);
        // put in the switching intent, the type of the action done. "Modif" if the next activity
        // must modify the selected couple, "Add" if the next activity must add a new couple
        intent.putExtra("action","add");

        // put a flag to specify the next activity like a new activity on this activity,
        // the menu of the app
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // start the new activity
        startActivity(intent);
    }
}